CFLAGS ?= -O2 -march=native
PREFIX ?= /usr
DESTDIR ?=
BINDIR ?= bin
MANDIR ?= share/man

CFLAGS += -std=c89 -pedantic -Wall -D_POSIX_C_SOURCE=200112L -D_XOPEN_SOURCE=500
OBJECTS = choice.o term.o
.PHONY: clean install
choice: $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) $(LDLIBS) -o $@
clean:
	rm -f choice $(OBJECTS)
D=$(DESTDIR:%=%/)$(PREFIX)
B=$(D)/$(BINDIR)
M=$(D)/$(MANDIR)
install: choice
	mkdir -p $(B) $(M)/man1
	cp choice $(B)/
	cp choice.1 $(M)/man1
